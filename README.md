# ssh-multi

Bash script using tmux to ssh into multiple servers and synchronize input across all panes

## Usage
Open tmux and run the script with a space separated list of servers as arguments. It's helpful to have ssh keys installed on all remote servers. If you have to enter a password on any of the remote systems, unsynchronize input across the panes or you could cleartext your password

## Examples
 Remember to have a tmux pane open before executing script
 ```
 # tmux
 # ./ssh-multi.sh lin1 lin2 lin3
 ```
 ```
 # tmux
 # ./ssh-multi.sh lin{1..3}
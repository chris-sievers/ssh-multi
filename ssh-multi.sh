#!/bin/bash

# Description:
#   script to ssh into multiple servers at the same time
#   and synchronize input across all terminals
# Usage:
#   1.  Make sure you have ssh-keys installed on all remote servers
#   2.  open tmux session on local server
#   3.  run script with list of servers separated by spaces
# Examples:
#   # ./ssh-multi.sh lin1 linserv1
#   # ./ssh-multi.sh lin{1..20}
#
#
# Written by Chris Sievers
# 7/10/2020
#

#read in entries from the command line when script is called
TARGETS=${TARGETS:=$*}

#if no hosts provided, ask again
check=1
while [ "$check" -eq 1 ]; do
  if [ -z "$TARGETS" ]; then
    echo -n "Please specify remote targets (separated by spaces): "
    read TARGETS
  else
    #exit loop
    check=0
  fi
done

#create array of target systems
targets=( $TARGETS )

#open new tmux window, ssh to first server, and remove from server list
tmux new-window "ssh ${targets[0]}"
unset targets[0];

#start splitting tmux pane for each subsequent server entry
for i in "${targets[@]}"; do
  tmux split-window -h "ssh $i"
  tmux select-layout tiled > /dev/null
done

#synchronize input across all panes
tmux select-pane -t 0
tmux set-window-option synchronize-panes on > /dev/null